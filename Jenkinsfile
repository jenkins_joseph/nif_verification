#!groovy

/*
 * CI on Jenkins
 * 20230510 Cesar Freire
 */

pipeline {
    agent none

    environment {
        HOME = "${env.WORKSPACE}"
    }

    stages {
        stage('Unit tests') {
            agent {
                docker {
                    image 'python:slim'
                    reuseNode true
                }
            }
            steps {
                sh"""
                export PIP_DISABLE_PIP_VERSION_CHECK=1
                export PYTHONDONTWRITEBYTECODE=1
                export PYTHONUNBUFFERED=1
                export PATH="${PATH}:$HOME/.local/bin"
                pip install --user -r requirements.txt
                pip install --user -r requirements-test.txt
                python3 -m pytest --junitxml results.xml tests/test_nif_validator.py
                """
            }
            post {
                always {
                    archiveArtifacts artifacts: 'results.xml', fingerprint: true
                    junit 'results.xml'
                }
            }
        }

        stage('CC') {
            agent {
                docker {
                    image 'python:slim'
                    reuseNode true
                }
            }
            steps {
                sh"""
                export PIP_DISABLE_PIP_VERSION_CHECK=1
                export PYTHONDONTWRITEBYTECODE=1
                export PYTHONUNBUFFERED=1
                export PATH="${PATH}:$HOME/.local/bin"
                pip install --user -r requirements.txt
                pip install --user -r requirements-test.txt
                python3 -m radon cc . -a -s --exclude site-packages
                """
            }
        }

        stage('Build') {
            agent any
            steps {
                sh 'docker build -t registry.gitlab.com/jenkins_joseph/nif_verification .'
            }
        }
        stage('Deliver') {
            agent any
            steps {
                withCredentials([usernamePassword(credentialsId: 'gitlab',
                passwordVariable:'gitlabPassword',
                usernameVariable: 'gitlabUser')])
                {
                    sh "docker login -u ${env.gitlabUser} -p ${gitlabPassword} registry.gitlab.com"
                    sh 'docker push registry.gitlab.com/jenkins_joseph/nif_verification'

                }
            }
        }
    }
}
